from django.shortcuts import render, HttpResponse
from .models import *
# Create your views here.
def index(request):
    #return HttpResponse('hola mundo')
    #varEstudiante = Estudiante.objects.get(nombres="Valeria", apellidos="Enri" )#.filter
    grupo_1 = Estudiante.objects.filter(grupo=1)

    grupo_4 = Estudiante.objects.filter(grupo=4)


    todos = Estudiante.objects.all()

    apd =  Estudiante.objects.values_list('apellidos', flat=True)
    r_apellidos = repetidos(apd.distinct(),  apd)
    rep_apellidos = todos.filter(apellidos__in = r_apellidos)

    ed = Estudiante.objects.values_list('edad', flat=True)
    r_edad = repetidos(ed.distinct(),  ed)
    rep_edad = todos.filter(edad__in = r_edad)

    g_3 =Estudiante.objects.filter(grupo = 3)
    ed_3 = g_3.values_list('edad', flat=True)
    r_edad_3 = repetidos(ed_3.distinct(),  ed_3)
    rep_edad_3 = g_3.filter(edad__in = r_edad_3)

    return render(request, 'index.html', {'grupo_1':grupo_1,
                                            'grupo_4':grupo_4,
                                            'rep_apellidos': rep_apellidos,
                                            'rep_edad': rep_edad,
                                            'rep_edad_3': rep_edad_3,
                                            'todos':todos})
def repetidos(l_u,l):
    uniq = {l_u[i]: 0 for i in range(0, len(l_u))}
    repetidos =[]
    for i in l:
        uniq[i] = uniq[i] + 1
        if uniq[i] > 1:
            repetidos.append(i)
    return repetidos